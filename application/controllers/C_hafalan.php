<?php 
class C_hafalan extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model ('model_santri');
		$this->load->model ('model_surah');
		$this->load->model ('model_ustadz');		
		$this->load->helper('url');
 
	}
 
	public function index(){
		$this->data['pendaftaran_tpq'] = $this->model_santri->get();
		$this->data['surah_alquran'] = $this->model_surah->get();
		$this->data['pilih_ustadz'] = $this->model_ustadz->get();
		$this->load->view('hafalan',$this->data);
	
	}


	public function aksi_upload(){

			$nama = $this->input->post('nama');
			$ustadz = $this->input->post('ustadz');
			$surah = $this->input->post('surah');
			$ayat = $this->input->post('ayat');
			$hafalan = $this->input->post('hafalan');
			$tajwid = $this->input->post('tajwid');
			$makhroj = $this->input->post('makhroj');
			$hasil_akhir = $this->input->post('hasil_akhir');
			$keterangan = $this->input->post('keterangan');
			$tanggal = $this->input->post('tanggal');

			$data = array(
				'nama' => $nama,
				'ustadz' => $ustadz,
				'surah' => $surah,
				'ayat' => $ayat,
				'hafalan' => $hafalan,
		 		'tajwid' => $tajwid,
		 		'makhroj' => $makhroj,
		 		'hasil_akhir' => $hasil_akhir,
		 		'keterangan' => $keterangan,
				'tanggal' => $tanggal,
								);
			$this->db->insert('hafalan',$data);
			$this->load->view('berandaAdmin');
		}
}