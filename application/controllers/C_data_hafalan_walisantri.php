<?php 
class C_data_hafalan_walisantri extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('M_hafalan_walisantri');
		$this->load->helper('url');
	}
	public function index(){
		$data['data_hafalan_walisantri'] = $this->M_hafalan_walisantri->get_user_all();
        $this->load->view('data_hafalan_walisantri',$data);
	}
 
}