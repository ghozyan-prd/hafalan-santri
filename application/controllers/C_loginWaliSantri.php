<?php  
class C_loginWaliSantri extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->model('M_login');
 
	}
 
	function index(){
		$this->load->view('loginWaliSantri');
	}
 
	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
			);
		$cek = $this->M_login->cek_login("walisantri",$where)->num_rows();
		if($cek > 0){
 
			$data_session = array(
				'nama' => $username,
				'status' => "loginWaliSantri"
				);
 
			$this->session->set_userdata($data_session);
 
			redirect(base_url("index.php/C_berandaWalisantri")); //jika benar masuk ke mustahiq
 
		}else{
			$this->load->view('V_error');
		}
	}
 
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('index.php/C_loginWaliSantri'));
	}
}