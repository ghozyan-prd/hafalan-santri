<?php 
class C_data_walisantri extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('M_waliSantri');
		$this->load->helper('url');
	}
	public function index(){
		$data['waliSantri'] = $this->M_waliSantri->get_user_all();
        $this->load->view('data_walisantri', $data);
	}
 
}