<?php 
class C_santri extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('M_santri');
		$this->load->helper('url');
	}
	public function index(){
		$data['santri'] = $this->M_santri->get_user_all();
        $this->load->view('santri', $data);
	}
 
}