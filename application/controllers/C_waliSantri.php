<?php 
class C_waliSantri extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model ('model_waliSantri');		
		$this->load->helper('url');
 
	}
 
	public function index(){
		$this->data['waliSantri'] = $this->model_waliSantri->get();
		$this->load->view('waliSantri',$this->data);
	
	}


	public function aksi_upload(){
			$nama = $this->input->post('nama');
			$santri = $this->input->post('santri');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$alamat = $this->input->post('alamat');
			$telp = $this->input->post('telp');
			$email = $this->input->post('email');

			$data = array(
				'nama' => $nama,
				'santri' => $santri,
				'username' => $username,
				'password' => md5($password),
				'alamat' => $alamat,
				'telp' => $telp,
				'email' => $email,

								);
			$this->db->insert('waliSantri',$data);
			$this->load->view('berandaAdmin');
		}
 }