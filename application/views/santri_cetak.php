<html>
<head>
	<title>DATABASE DATA SANTRI</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/santri1.css'); ?>">
	<link rel="icon" href="<?php echo base_url('assets/images/sippekat.png'); ?>">
</head>
<body>
	<div id="header">
		<div class="sippekat">SIM SANTRI</div>
		<hr color="#EBC64E" size="7" class="garis">
		<div class="sippekat1">Sistem Informasi Hafalan Al-Qur'an Santri</div>
		<div class="login">
			<p class="login1">SANTRI</p>
		</div>
		<img src="<?php echo base_url('assets/images/sippekat.png'); ?>" class="logosip">
		<img src="<?php echo base_url('assets/images/sippekat1.png'); ?>" class="logosip1">
	</div>
	<div id="daftar">
		<h1 class="daftarmuz">DATABASE DATA SANTRI</h1>
	</div>
		<table>
			<tr>
			<th>No</th>
			<th>NAMA SANTRI</th>
			<th>JENJANG PENDIDIKAN</th>
			<th>ASAL SEKOLAH</th>
			<th>EMAIL</th>
			<th>TANGGAL LAHIR</th>
			<th>ALAMAT</th>
			<th>NO TELEPON</th>
			<th>JENIS KELAMIN</th>
			<th>PHOTO SANTRI</th>
		</tr>
		<?php 
		$no = 1;
		foreach($santri as $u){ 
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $u->nama ?></td>
			<td><?php echo $u->jenjang ?></td>
			<td><?php echo $u->sekolah ?></td>
			<td><?php echo $u->email ?></td>
			<td><?php echo $u->ttl ?></td>
			<td><?php echo $u->alamat ?></td>
			<td><?php echo $u->telp ?></td>
			<td><?php echo $u->jk ?></td>
			<td><img src="<?php echo base_url(); ?>./assets/images/pendaftaran_tpq/<?=$u->foto;?>"' width='200' height='150'></td> 
		<?php } ?>
		</tr>
		</table>
		<script >
			window.print();
		</script>
</body>
</html>