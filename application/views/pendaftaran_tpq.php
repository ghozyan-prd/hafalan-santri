<html>
<head>
	<title>REGISTRASI</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/pendaftaran.css'); ?>">
	<link rel="icon" href="<?php echo base_url('assets/images/sippekat.png'); ?>">
</head>
<body>
	<div id="header">
		<div class="sippekat">SIM SANTRI</div>
		<hr color="#EBC64E" size="7" class="garis">
		<div class="sippekat1">Sistem Informasi Hafalan Al-Qur'an Santri</div>
		<div class="login">
			<p class="login1">SANTRI</p>
		</div>
		<img src="<?php echo base_url('assets/images/sippekat.png'); ?>" class="logosip">
		<img src="<?php echo base_url('assets/images/sippekat1.png'); ?>" class="logosip1">
	</div>

	<div id="daftar">
	<h1>PENDAFTARAN SANTRI BARU</h1>
	
	</div>

	<div id="tabelmuz">
		<?php echo form_open_multipart('index.php/C_pendaftaran_tpq/aksi_upload');?>
		<label for="nama" class="nama">NAMA SANTRI</label>
		<input type="text" name="nama" class="inputnama" required>
		<label for="nama" class="jenjang">JENJANG PENDIDIKAN</label>
		
		<div >
		<select  class="inputpendidikan" name="jenjang"> <option value='' disabled selected>------------------Jenjang Pendidikan----------------</option>
		<option  value="TK Sederajat">TK Sederajat</option>
    	<option value="SD Sedeeajat">SD Sederajat</option>
    	<option value="SMP Sederajat">SMP Sederajat</option>
    	<option value="SMA Sederajat">SMA Sederajat</option>
    	<option value="Mahasiswa">MAHASISWA</option>
  		</select>
  		</div>  

		<label for="sekolah" class="sekolah">ASAL SEKOLAH</label>
		<input type="text" name="sekolah" class="inputsekolah" required pattern=".{8,}">

		<label for="nama" class="email">EMAIL</label>
		<input type="email" name="email" class="inputemail" required>

		<label for="ttl" class="ttl">TANGGAL LAHIR</label>
		<input type="date" name="ttl" class="inputtl" required>

		<label for="alamat" class="alamat">ALAMAT</label>
		<textarea name="alamat" class="inputala" required></textarea>

		<label for="telepon" class="telp">TELEPON</label>
		<input type="tel" name="telp" class="inputtel"  required>

		<label for="jk" class="jk">JENIS KELAMIN</label>
		<div class="pilihan">
			<input type="radio" name="jk" value="Laki-laki" style="width: 15px;height: 15px;"> Laki-Laki
			<input type="radio" name="jk" value="Perempuan" style="width: 15px;height: 15px;"> Perempuan
		</div>

		<div class="fotop">
			<p class="foto1">FOTO</p>
			<input type="file" name="userfile" accept="image/*" class="pilihf1" required>
		</div>

		<button class="btsubmit" type="submit" value="DAFTAR">DAFTAR</button>
		<button class="btbatal" type="reset" value="Batal">BATAL</button>
	</div>
	</form>
</body>
</html>