<html>
<head>
	<title>REGISTRASI</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/guruNgaji.css'); ?>">
	<link rel="icon" href="<?php echo base_url('assets/images/sippekat.png'); ?>">
</head>
<body>
	<div id="header">
		<div class="sippekat">SIM SANTRI</div>
		<hr color="#EBC64E" size="7" class="garis">
		<div class="sippekat1">Sistem Informasi Hafalan Al-Qur'an Santri</div>
		<div class="login">
			<p class="login1">SANTRI</p>
		</div>
		<img src="<?php echo base_url('assets/images/sippekat.png'); ?>" class="logosip">
		<img src="<?php echo base_url('assets/images/sippekat1.png'); ?>" class="logosip1">
	</div>

	<div id="daftar">
	<h1>PENDAFTARAN AKUN WALI SANTRI</h1>
	
	</div>

	<div id="tabelmuz">
		<?php echo form_open_multipart('index.php/C_guruNgaji/aksi_upload');?>
		<label for="nama" class="nama">NAMA GURU NGAJI</label>
		<input type="text" name="nama" class="inputnama" required>
		
		<label for="username" class="username">USERNAME</label>
		<input type="username" name="username" class="inputuser" required>

		<label for="password" class="password">PASSWORD</label>
		<input type="password" name="password" class="inputpassword" required>

		<label for="alamat" class="alamat">ALAMAT</label>
		<textarea name="alamat" class="inputalamat" required></textarea>

		<label for="telepon" class="telp">TELEPON</label>
		<input type="tel" name="telp" class="inputtelp" required>

		<label for="nama" class="email">EMAIL</label>
		<input type="email" name="email" class="inputemail" required>

	

		<button class="btsubmit" type="submit" value="DAFTAR">DAFTAR</button>
		<button class="btbatal" type="reset" value="Batal">BATAL</button>
	</div>
	</form>
</body>
</html>