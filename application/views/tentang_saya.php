<html>
<head>
	<title>SIM SANTRI</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/tentang_saya.css'); ?>">
	<link rel="icon" href="<?php echo base_url('assets/images/sippekat1.png'); ?>">
</head>
<body>
	<div id="awal">
		<div class="sippekat">SIM SANTRI</div>
		<hr color="#EBC64E" size="7" class="garis">
		<div class="sippekat1">Sistem Informasi Hafalan Al-Qur'an Santri</div>
		<img src="<?php echo base_url('assets/images/sippekat.png'); ?>" class="logosip">
		<img src="<?php echo base_url('assets/images/sippekat1.png'); ?>" class="logosip1">
	</div>
	<p class="selamat">TENTANG SAYA</p>
	<hr color="white" size="5" class="garis2">

	<p class="selamat3">NAMA</p>
	<p class="selamat4">:</p>
		<p class="saya1">GHOZYAN HILMAN PRADANA</p>

	<p class="selamat5">NIM</p>
	<p class="selamat6">:</p>
		<p class="saya2">H06216008</p>

	<p class="selamat7">TTL</p>
	<p class="selamat8">:</p>
		<p class="saya3">NGAWI, 21 MARET 1998</p>

	<p class="selamat9">ALAMAT</p>
	<p class="selamat10">:</p>
		<p class="saya4">Kecamatan Jogorogo <br> Kabupaten Ngawi <br> Jawa Timur <br> Indonesia</p>

	<p class="selamat11">TELEPON</p>
	<p class="selamat12">:</p>
		<p class="saya5">081273338544 (Whatsapp and Line)</p>

	<p class="selamat13">E-MAIL</p>
	<p class="selamat14">:</p>
		<p class="saya6">@ghozyan20.ozy@gmail.com</p>

	<p class="selamat15">INSTAGRAM</p>
	<p class="selamat16">:</p>
		<p class="saya7">@ghzyan</p>

	<p class="selamat17">PHOTO</p>
	<p class="selamat18">:</p>
		<img src="<?php echo base_url('assets/images/ghozyan.png'); ?>" class="saya8">
		<img src="<?php echo base_url('assets/images/ghozyan1.png'); ?>" class="saya9">


	<p class="riwayat">RIWAYAT PENDIDIKAN</p>
	<hr color="white" size="5" class="skl0">

	<p class="riwayat3">SD</p>
	<p class="riwayat4">:</p>
		<p class="skl1">SEKOLAH DASAR NEGERI 3 JOGOROGO</p>
		<p class="skl2">Tahun 2004 sd Tahun 2010</p>

	<p class="riwayat5">SMP</p>
	<p class="riwayat6">:</p>
		<p class="skl3">SEKOLAH MENENGAH PERTAMA NEGERI 1 JOGOROGO</p>
		<p class="skl4">Tahun 2010 sd Tahun 2013</p>

	<p class="riwayat7">SMA</p>
	<p class="riwayat8">:</p>
		<p class="skl5">SEKOLAH MENENGAH ATAS NEGERI 1 NGAWI</p>
		<p class="skl6">Tahun 2013 sd Tahun 2016</p>

	<p class="riwayat9">KULIAH</p>
	<p class="riwayat10">:</p>
		<p class="skl7">INFORMATION SYSTEM  TAHUN 2016<br> FACULTY OF SCIENCE AND TECHNOLOGY <br> UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</p>



	
</body>
</html>